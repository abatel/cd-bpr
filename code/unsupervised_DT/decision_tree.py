import argparse
import pandas as pd
import numpy as np
import csv
from matplotlib import gridspec
from sklearn.manifold import TSNE
import seaborn as sns
from sklearn import preprocessing
import matplotlib.pyplot as plt
from utils import *
from sklearn.metrics import silhouette_score, silhouette_samples
from sklearn.tree import DecisionTreeClassifier, export_text
import os

def new_silhouette(X, cluster_labels):   
    silhouette_avg = silhouette_score(X, cluster_labels)
    return silhouette_avg
   
class Tree:
    def __init__(self, feature, threshold, feats, users, selected_features):
        self.children_left = None
        self.children_right = None
        self.feature = None #feature
        self.threshold = None #threshold
        self.attributes = feats 
        self.feat_selected = selected_features
        self.users = users
        self.users_quest = []
    
    def apply_feature(self, feature, threshold, H_users, extend, min_users):
        self.feature = feature
        self.threshold = threshold
        h = H_users[:,feature]
        part = h <= threshold
        part = (1 * part).reshape(-1)
        intersection = self.users * part
        new_feats = self.attributes.copy()
        new_feats.remove(feature)
        new_selected_feats = self.feat_selected.copy()
        new_selected_feats.append(feature)
        right_users = self.users - intersection
        if (extend == True):
            if(np.sum((intersection)) >= min_users) and (np.sum((right_users)) >= min_users):
                self.children_left = Tree(feature, threshold, new_feats, intersection, new_selected_feats)
                self.children_right = Tree(feature, threshold, new_feats, right_users, new_selected_feats)
            else:
                self.children_left = None
                self.children_right = None
            return self.children_left, self.children_right
        else:
            return intersection, right_users
      
    def findBestAttribute(self, partitions, H_users, min_users):
        f = np.array(self.feat_selected).reshape(-1)
        X = H_users[self.users == 1,:]
        X = X[:,f]
        iBestAtt = -1
        best_attribute = 0
        for i in self.attributes:
            a = -1
            b = -1
            # We test the two classes
            feature = int(partitions[i,0])
            threshold = partitions[i,1]
            left_users, right_users = self.apply_feature(feature, threshold, H_users, False, min_users)
            group1 = left_users[self.users == 1]
            group2 = right_users[self.users == 1]
            if(np.sum(group1) > 0) and (np.sum(group1) < np.sum(self.users)): 
                a = new_silhouette(X, group1)
            if(np.sum(group2) > 0) and ( np.sum(group2) < np.sum(self.users)):
                b = new_silhouette(X, group2)
            if(iBestAtt == -1) or (a > best_attribute):
                iBestAtt = i
                best_attribute = a
            if(iBestAtt == -1) or (b > best_attribute):
                iBestAtt = i
                best_attribute = b
        return iBestAtt           

def compute_initial_partition(h, n):
    h = h.reshape(-1,1)
    # Sort data
    h_sort, ind_sort = zip(*sorted(zip(h, list(range(len(h))))))
    # Find threshold
    limits = int(len(h) * 20/100.0)
    labels = np.full(len(h),0)
    max_silhouette = 0.0
    max_split = 0.0
    for i in range(0, limits):
        labels[ind_sort[i]] = 1
    for i in range(limits, len(h)-limits):
        labels[ind_sort[i]] = 1
        silhouette_avg = new_silhouette(h, labels)
        if(silhouette_avg > max_silhouette):
            max_silhouette = silhouette_avg 
            max_split = i
            best_part = np.copy(labels)
    threshold = h_sort[max_split]
    # Reconstruct the partition:
    part = h <= threshold[0]
    part = (1 * part).reshape(-1)
    return float(threshold[0]), max_silhouette 

def tree_traversal(tree, min_users, dim):
    if(tree == None) or (len(tree.feat_selected) == dim ):
        return
    else:
        #left
        feature = tree.findBestAttribute(partitions_init, H_users, min_users)
        if(feature != -1):
            threshold = partitions_init[feature,1]
            treeL, treeR = tree.apply_feature(feature, threshold, H_users, True, min_users)
            tree_traversal(treeL, min_users, dim)
            tree_traversal(treeR, min_users, dim)

def print_node(tree, level, pref, gauche, theClusters):
    first = 0
    global nbc
    if(tree.threshold != None):
        th = "%.2f" % tree.threshold
    else:
        th = "nan"
    effectif = '' 
    if gauche:
        sign = '<='
        if(tree.children_left != None):
            effectif = str(np.sum(tree.children_left.users))
        else:
            if(tree.children_right != None):
                effectif = str(' cluster ') + str(nbc) + " ("+ str(np.sum(tree.users) - np.sum(tree.children_right.users))+")"
                theClusters.append((len(theClusters)+1)* (tree.users-tree.children_right.users))
                print("|  "+pref + ""*level+ "---"+ effectif)
                effectif = ''
                nbc = nbc + 1
    else:       
        sign = '>'
        if(tree.children_right != None):
            effectif = str(np.sum(tree.children_right.users))
        else:
            if(tree.children_left != None):
                effectif = str(' cluster ') + str(nbc) + " ("+ str(np.sum(tree.users) - np.sum(tree.children_left.users))+")"
                nbc = nbc + 1
                theClusters.append((len(theClusters)+1)* (tree.users-tree.children_left.users))
                first = 1
    if(tree.children_left == None) and (tree.children_right == None):
        effectif = str(' cluster ') + str(nbc) + " ("+  str(np.sum(tree.users))+")"
        theClusters.append((len(theClusters)+1)*tree.users)
        if(gauche):
            print("|  "+pref + "---"+ effectif)
            nbc = nbc + 1
            first = 1
            return 0
    if(first == 0):
        print("|  "+pref +  "--- feature_"+str(tree.feature)+ " "+sign+" " +str(th))
    else:
        print("|  "+pref + "--- feature_"+str(tree.feature)+ " "+sign +" "+str(th))
        print("|  "+pref + "|  "+ "---")#+ effectif)
    return 1


def print_s(vect):
    s = '['
    for i in range(len(vect)-1):
        if(vect[i] == 1):
            s = s + str(i) + ", "
    if(vect[len(vect)-1] == 1):
        s = s + str(len(vect)-1)
    s = s +']'
    s = ''
    return s

def print_nodeEval(tree, level, pref, gauche, questClusters):
    first = 0
    if(tree.threshold != None):
        th = "%.2f" % tree.threshold
    else:
        th = "nan"
    effectif = '' 
    if gauche:
        sign = '<='
        if(tree.children_left != None):
            effectif = str(np.sum(tree.children_left.users))
        else:
            if(tree.children_right != None):
                questClusters.append(tree.users_quest)
                diff = tree.users - tree.children_right.users
                effectif = str('class ') + str(np.sum(tree.users) - np.sum(tree.children_right.users)) + print_s(diff)
                theClusters.append((len(theClusters)+1) * (tree.users-tree.children_right.users))
                #print("|  "+pref + "---"+ effectif)
                effectif = ''
    else:       
        sign = '>'
        if(tree.children_right != None):
            effectif = str(np.sum(tree.children_right.users))
        else:
            if(tree.children_left != None):
                questClusters.append(tree.users_quest)
                diff = tree.users - tree.children_left.users
                effectif = str('class ') + str(np.sum(tree.users) - np.sum(tree.children_left.users)) + print_s(diff)
                theClusters.append((len(theClusters)+1)* (tree.users-tree.children_left.users))
                first = 1
    if(tree.children_left == None) and (tree.children_right == None):
        questClusters.append(tree.users_quest)
        effectif = str('class ') + str(np.sum(tree.users))+ print_s(tree.users_quest)+print_s(tree.users)
        theClusters.append((len(theClusters)+1)*tree.users)
        if(gauche):
            #print("|  "+pref + "---"+ effectif)
            first = 1
            return 0
    '''
    if(first == 0):
        print("|  "+pref +  "--- feature"+str(tree.feature)+ sign +str(th) + "  "+ effectif) #+ print_s(tree.users_quest))
    else:
        print("|  "+pref +  "--- feature"+str(tree.feature)+ sign +str(th))#+ print_s(tree.users_quest))
        print("|  "+pref + "|  "+ "---"+ effectif) 
    '''
    return 1

def showTreeEval(tree,level, pref, theClusters):
    if(tree == None):
        return
    else:
        r = print_nodeEval(tree, level, pref, True, theClusters)
        if(r == 1):
            showTreeEval(tree.children_left,level+1, pref+"|  ", theClusters)
            r =  print_nodeEval(tree, level, pref, False, theClusters)
            showTreeEval(tree.children_right,level+1,  pref+"|  ", theClusters  )

def showTree(tree,level, pref, theClusters):
    if(tree == None):
        return
    else:
        r = print_node(tree, level, pref, True, theClusters)
        if(r == 1):
            showTree(tree.children_left,level+1, pref+"|  ", theClusters)
            r = print_node(tree, level, pref, False, theClusters)
            showTree(tree.children_right,level+1,  pref+"|  ", theClusters)
           
def tree_eval(tree, quest_user, user_id, questClusters, lower):
    if(tree == None):
        return
    else:
        feat = tree.feature
        if(lower == 0):
            if(feat != None) and ((quest_user[feat] == 1) or (quest_user[feat] == -1) or (quest_user[feat] == 0)): 
                if(tree.children_left != None):
                    tree_eval(tree.children_left, quest_user, user_id, questClusters, lower)
                else:
                    tree.users_quest.append(user_id)
        else:
            if(feat != None) and ((quest_user[feat] == 1) or (quest_user[feat] == -1)):# or (quest_user[feat] == 0)): 
                if(tree.children_left != None):
                    tree_eval(tree.children_left, quest_user, user_id, questClusters, lower)
                else:
                    tree.users_quest.append(user_id)

        if(lower == 0):
            if(feat != None) and ((quest_user[feat] == 1) or (quest_user[feat] == -1) or (quest_user[feat] == 2)): 
                if(tree.children_right != None):
                    tree_eval(tree.children_right, quest_user, user_id, questClusters, lower)
                else:
                    if(tree.children_left != None):
                        tree.users_quest.append(user_id)
        else:
            if(feat != None) and ((quest_user[feat] == 1) or (quest_user[feat] == -1)):# or (quest_user[feat] == 2)): 
                if(tree.children_right != None):
                    tree_eval(tree.children_right, quest_user, user_id, questClusters, lower)
                else:
                    if(tree.children_left != None):
                        tree.users_quest.append(user_id)

def compare_clusters(dtClusters, questClusters, quest):
    count = 0
    homogene = np.full((len(questClusters), len(quest[0]),4), 0)
    for k in range(len(questClusters)):
        #print(np.sum(dtClusters == (k+1)), len(questClusters[k]))
        for i in range(len(dtClusters)):
            if(dtClusters[i] == (k+1)) and (i in questClusters[k]):
                count = count + 1
                for d in range(len(quest[0])):
                    v = int(quest[i, d])
                    if(v != -1):
                        homogene[k][d][v] = homogene[k][d][v] + 1
        #print(count)
    #print("negatifs")
    #print(homogene[:,:,0])
    #print("positifs")
    #print(homogene[:,:,2])
    print("Purity:",count / float(len(dtClusters)))
    
def print_gr(gr):
    print(gr)
    
## main
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-lower", "--lower", help="int")
    #parser.add_argument("-dt", "--dataTest", help="data file")
    args = parser.parse_args()
    lower = int(args.lower)

min_users = 50
os.getcwd()
path = os.path.dirname(os.path.realpath(__file__))
print(path+ "/files_for_dt/train_embed.csv")
H_users = fromDFtoArray(path+ "/files_for_dt/train_embed.csv",False,'f')
dim = H_users.shape[1]
n = H_users.shape[0]

'''
# step 1: compute initial partitions
partitions = []
for j in range(dim): 
    threshold, max_silhouette = compute_initial_partition(H_users[:,j], n)
    partitions.append([j, threshold, max_silhouette])
    #comput_init_part(H_users[:,j], n)
    best_part = H_users[:,j] < threshold
    best_part = (1 * best_part).reshape(-1)
    print(threshold, np.sum(best_part), n - np.sum(best_part), max_silhouette)
# Save the partitions to next time
nom = path+"/xx_partition.csv"
print(nom)
f = open(nom, 'w')
writer = csv.writer(f)
for i in range(len(partitions)):
    row = partitions[i]
    writer.writerow(row)
f.close()
'''
partitions_init = fromDFtoArray(path+ "/files_for_dt/train_partitions.csv",False,'f')

# Step 2: take the partition with max silhouette value
best_init_part = np.argmax(partitions_init[:,2])
feature = int(partitions_init[best_init_part,0])
threshold = partitions_init[best_init_part,1]
all_users = np.full(n,1)
root = Tree(None, None, list(range(dim)), all_users, [])
root.apply_feature(feature, threshold, H_users, True, min_users)

# Step3 : apply the partitions
tree = root
tree_traversal(tree.children_left, min_users, dim)
tree_traversal(tree.children_right, min_users, dim)

#Step 4: vizualization
theClusters = []
nbc = 0
showTree(tree,0,"", theClusters)
'''
nbc = 0
showTreeBis(tree,0,"",1)#level, pref)
nbc = 0
tree_traversal_print(tree, 0, 0)
#tree_rules = export_text(tree, feature_names=list(range(dim)))
'''

'''
for i in range(2):
    #right
    feature = tree.children_right.findBestAttribute(partitions_init, H_users)
    threshold = partitions_init[feature,1]
    tree = tree.children_right.apply_feature(feature, threshold, H_users, True)
    #tree = tree.children_right
'''

# Compute clusters
dtClust = theClusters[0]
#print(0,np.sum((dtClust==0)))
for i in range(1,len(theClusters)):
    dtClust = dtClust + theClusters[i]
    print_gr(np.where(dtClust==i))
#print(dtClust)

# Validation with questionnaires
print("")
file = path+ "/files_for_dt/train_user_quest_label.csv"
quest = fromDFtoArray(file,False,'f')
questClusters = []
for i in range(len(quest)):
    # we got a user
    # let us consider the tree
    # print(quest[i])
    tree_eval(root, quest[i], i, [], lower)

# Computation of questClusters
showTreeEval(tree,0,"", questClusters)
compare_clusters(dtClust, questClusters, quest)

#print(len(questClusters))
#print("les seuils")
#print(partitions_init[:,1])


#print(partitions_init)
    
