# CD-BPR
##### A simple yet effective interpretable Bayesian Personalized Ranking for the Assessment of Psychiatric Disorders, 2024

---

The current repository contains all the code and data necessary to reproduce the paper results. 

## Installation

Easily clone the repository with `git` and set up the environment (data and libraries) with `make` command :  

```
git clone https://gitlab.liris.cnrs.fr/abatel/cd-bpr.git
cd path/to/code
make
```

Prerequisite : [conda library]( https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)\
Attention: It is recommended to uninstall the pytorch library provided with the conda environment to download yourself the version most adapted to your machine from the pytorch [website](https://pytorch.org/get-started/locally/).

## Reproductibility

All the commands needed to reproduce the paper results are written in the `Experiments.ipynb` jupyter notebook at the root of the directory.
## Authors
C. Robardet, A. Batel, M. Plantevit, I. Benouaret
>LIRIS laboratory, UMR5205, Univ Lyon, CNRS, UBL,LIRIS
>EPITA Research Laboratory (LRE)
